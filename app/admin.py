from django.contrib import admin
from .models import Transaction


class TransactionAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'value', 'positive', 'created_at']
    readonly_fields = ['id']
    ordering = ['-created_at']


# Register your models here.
admin.site.register(Transaction, TransactionAdmin)
