from rest_framework import viewsets

from .models import Transaction
from .serializers import TransactionSerializer
from drf_spectacular.utils import extend_schema


# Create your views here.
@extend_schema(
    tags=['momaba external apis']
)
class TransactionViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all().order_by('-created_at')
    serializer_class = TransactionSerializer
